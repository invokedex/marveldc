<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    function list()
    {
        return view('movie',[
            'movies' => Movie::all()
        ]);
    }

    function showById(Movie $movie)
    {
        return view('movieOne', [
            'movieOne' => $movie
        ]);
    }

    function search(Movie $movie)
    {
        $film = DB::table('movies')->where('id', $movie->id)->first();
        return view('welcome',[
            'movieSearch' => $film
        ]);
    }

}
